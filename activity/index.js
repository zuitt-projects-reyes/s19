/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
	2. Create a class constructor able to receive 3 arguments
		-It should be able to receive two strings and a number
		-Using the this keyword assign properties:
			name, 
			breed, 
			dogAge = <7x human years> 
			-assign the parameters as values to each property.

	Create 2 new objects using our class constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.


*/

// For #1
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}

const introduce = student => {
	//Note: You can destructure objects inside functions.
	const { name, age, classes } = student1;

	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses ${classes}.`);
}

introduce(student1);

const getCube = num => Math.pow(num, 3);

let cube = getCube(3);

console.log(cube);

let numArr = [15,16,32,21,21,2];

numArr.forEach(num => console.log(num));

let numsSquared = numArr.map(num => num ** 2);

console.log(numsSquared);

// For #2

class Dog {
	constructor(name, breed, humanYears) {
		this.name = name;
		this.breed = breed;
		this.dogAge = humanYears * 7;
	}
}

const dog1 = new Dog('Dog 1', 'Golden Retriever', 1);
const dog2 = new Dog('Dog 2', 'Golden Retriever', 2);

console.log(dog1);
console.log(dog2);
